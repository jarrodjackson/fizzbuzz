import json
from flask import Flask

application = Flask(__name__)


def get_fizzy(num, endpoint):
    # TODO: specify upper limit in documentation
    #if int(num) not in range(1,501):
    #    return 'Out of bounds'
    it = '' if num % 3 else 'Fizz'
    if num % 5 == 0:
        it += 'Buzz'
    if it:
        return it
    else:
        return num


@application.route('/fizzbuzz/<num>')
def fizzbuzz(num):
    results = []
    for i in range(1, int(num)):
        result = get_fizzy(int(i), 'FizzBuzz')
        results.append(result)
    resp_body = results
    status = 200

    response = application.response_class(
        response=json.dumps(resp_body),
        status='{0}'.format(status),
        mimetype='application/json'
    )
    return response


if __name__ == "__main__":
    application.run(host='0.0.0.0', port='80')
