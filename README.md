# FizzBuzz

FizzBuzz microservice which returns the FizzBuzz output as an array.

## Usage

```
docker build -t fizzer .
docker run -p 80:80 fizzer
```
